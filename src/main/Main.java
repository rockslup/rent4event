package main;

import display.DisplayInicio;
import entidades.Cliente;
import entidades.Endereco;
import entidades.Local;
import entidades.Proprietario;
import gerenciadores.GerenciadorAluguel;
import gerenciadores.GerenciadorCliente;
import gerenciadores.GerenciadorLocal;
import gerenciadores.GerenciadorProprietario;

public class Main {

	public static void main(String[] args) {
		GerenciadorCliente gerCliente = new GerenciadorCliente();
		GerenciadorProprietario gerProprietario = new GerenciadorProprietario();
		GerenciadorLocal gerLocal = new GerenciadorLocal();
		GerenciadorAluguel gerAluguel = new GerenciadorAluguel();

		/*
		 * Para facilitar os testes, instaciei alguns objetos
		 */
		Proprietario p1 = new Proprietario("Luana", "47672753851", "123", "luana@live.com", "48646848664", "54654544554");
		gerProprietario.addEntidade(p1);
		Cliente c1 = new Cliente("Rafa", "78246442864", "123", "guerra@live.com", "1556166545");
		gerCliente.addEntidade(c1);
		Endereco e1 = new Endereco("Rua paraiso", "215", "56545644", "hell", "city", "province", "country");
		Local l1 = new Local("Chacara dos top", 21, true, p1, e1);
		gerLocal.addEntidade(l1);
		Endereco e2 = new Endereco("Rua 2", "452", "2442454454545", "dfgd", "eggeer", "proviegegnce", "regergge");
		Local l2 = new Local("Chacara dos deusos", 81, true, p1, e2);
		gerLocal.addEntidade(l2);
		
		
		int operacao = 0;

		do {
			operacao = DisplayInicio.mostrarMenuInicial();

			switch (operacao) {
			case 1:
				gerCliente.cadastrarCliente();
				break;
			case 2:
				gerProprietario.cadastrarProprietario();
				break;
			case 3:
				gerCliente.acessarCliente(gerLocal, gerAluguel);
				break;
			case 4:
				gerProprietario.acessarProprietario(gerLocal);
				break;
			case 0:
				System.out.println("Obrigado por utilizar o programa!");
				break;
			default:
				System.out.println("Opcao invalida!");
			}

		} while (operacao != 0);
	}
}
