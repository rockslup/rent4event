package entidades;

import java.util.ArrayList;
import java.util.Date;

public class Agenda {
	private static ArrayList<Aluguel> listaAlugueis = new ArrayList<Aluguel>();

	public static ArrayList<Aluguel> getListaAlugueis() {
		return listaAlugueis;
	}
	
	public static void addAluguel(Aluguel a) {
		listaAlugueis.add(a);
	}
	
	public static void removeAluguel(Aluguel a) {
		listaAlugueis.remove(a);
	}
	
	public static Aluguel findAluguelByCliente(Cliente c) {
		
		for(Aluguel aluguel : listaAlugueis) {
			if(aluguel.getCliente().equals(c))
				return aluguel;
		}
		return null;
	}
	
	public static boolean isValid(Aluguel a, Aluguel b) {
		Date inicioReservada = a.getDtEntrada();
		Date fimReservada 	 = a.getDtSaida();
		
		Date entrada = b.getDtEntrada();
		Date saida 	 = b.getDtSaida();
		
		if(entrada.after(inicioReservada) && entrada.before(fimReservada))
			return false;
		else
			if(saida.after(inicioReservada) && saida.before(fimReservada))
				return false;
		return true;
	}
	
	public static void findAluguelByDateAndPlace(Aluguel a) throws Exception {
		Local l = a.getLocal();
		boolean isAlugado = false;
		
		for(Aluguel aluguel : listaAlugueis) {
			if(l.equals(aluguel.getLocal())) {
				if(!isValid(aluguel, a)) {
					isAlugado = true;
					break;
				}
			}
		}
		
		if(!isAlugado)
			addAluguel(a);
		else
			throw new Exception("Local ja reservado para a data escolhida");
	}
}
