package entidades;
import java.util.ArrayList;

import interfaces.Imprimivel;

public class Local implements Imprimivel {
	
	private static int sequence = 0;
	
	private ArrayList<Aluguel> alugueis = new ArrayList<Aluguel>();
	private final int ID;
	private String nome;
	private double preco;
	//private int tempoMaxAluguel;
	private ArrayList<Double> avalicaoes = new ArrayList<Double>();
	private Endereco endereco;
	private boolean status; /*
		disponivel ou indisponivel
	*/
	private Proprietario proprietario;
	
	public Local(String nome, double preco, boolean status, Proprietario proprietario) {
		this.ID = ++(sequence);
		this.setNome(nome);
		this.setPreco(preco);
		this.setStatus(status);
		this.setProprietario(proprietario);
		proprietario.addLocal(this);
	}
	
	public Local(String nome, double preco, boolean status, Proprietario proprietario, Endereco end) {
		this.ID = ++(sequence);
		this.setNome(nome);
		this.setPreco(preco);
		this.setStatus(status);
		this.setEndereco(end);
		this.setProprietario(proprietario);
		proprietario.addLocal(this);
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the preco
	 */
	public double getPreco() {
		return preco;
	}

	/**
	 * @param preco the preco to set
	 */
	public void setPreco(double preco) {
		this.preco = preco;
	}

	/**
	 * @return the avalicaoes
	 */
	public ArrayList<Double> getAvalicaoes() {
		return avalicaoes;
	}

	/**
	 * @param avalicaoes the avalicaoes to set
	 */
	public void setAvalicaoes(ArrayList<Double> avalicaoes) {
		this.avalicaoes = avalicaoes;
	}

	/**
	 * @return the endereco
	 */
	public Endereco getEndereco() {
		return endereco;
	}

	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/**
	 * @return the status
	 */
	public boolean getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}


	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @return the proprietario
	 */
	public Proprietario getProprietario() {
		return proprietario;
	}

	/**
	 * @param proprietario the proprietario to set
	 */
	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}
	
	public String toString() {
		String saida = "";
		saida += "ID: " + this.getID() + "\n";
		saida += "Nome: " + this.getNome() + "\n";
		saida += "Preco/hora: " + this.getPreco() + "\n";
		saida += "Status: " + (this.getStatus() ? "Disponivel" : "Indisponivel") + "\n";
		saida += this.endereco.toString();
		//saida += "Avaliacoes: " + this.avalicaoes.toString() + "\n";
		return saida;
	}

	public ArrayList<Aluguel> getAlugueis() {
		return alugueis;
	}

	public void setAlugueis(ArrayList<Aluguel> alugueis) {
		this.alugueis = alugueis;
	}
	
	public void addLocal(Aluguel alug) {
		this.alugueis.add(alug);
	}
	
	public void removeLocal(Aluguel alug) {
		this.alugueis.remove(alug);
	}
	
	public void addAvaliacao(double nota) {
		this.avalicaoes.add(nota);
	}

	@Override
	public void imprimeDados() {
		System.out.println(this.toString());
		
	}
}
