package entidades;
import java.util.ArrayList;

import interfaces.Imprimivel;


public class Proprietario extends Usuario implements Imprimivel {
	
	private ArrayList<Local> locais = new ArrayList<Local>();
	
	public Proprietario(String nome, String cpf, String senha, String email, String tel) {
		super(nome, cpf, senha, email, tel);
	}
	
	public Proprietario(String nome, String cpf, String senha, String email, String tel, String numConta) {
		super(nome, cpf, senha, email, tel, numConta);
	}

	/**
	 * @return the locais
	 */
	public ArrayList<Local> getLocais() {
		return locais;
	}

	/**
	 * @param locais the locais to set
	 */
	public void setLocais(ArrayList<Local> locais) {
		this.locais = locais;
	}
	
	public void addLocal(Local l) {
		this.locais.add(l);
	}
	
	public void removeLocal(Local l) {
		this.locais.remove(l);
	}
	
	public void alterarLocal(Local l, String nome, double preco, boolean status, String log, String numero, String bairro, String cidade, String estado, String pais) {
		l.setNome(nome);
		l.setPreco(preco);
		l.setStatus(status);
		l.getEndereco().setLogradouro(log);
		l.getEndereco().setNumero(numero);
		l.getEndereco().setBairro(bairro);
		l.getEndereco().setCidade(cidade);
		l.getEndereco().setEstado(estado);
		l.getEndereco().setPais(pais);
	}
	
	/*
	 * retorna lista de locais do seu atributo ArrayList<Local> locais, com nome like nome
	 * */
	public ArrayList<Local> getLocalByNome(String nome) {
		ArrayList<Local> retorno = new ArrayList<Local>();
		for(Local l: this.locais) {
			if(l.getNome().toLowerCase().contains(nome.toLowerCase())) {
				retorno.add(l);
			}
		}
		return retorno;
	}
	
	/*
	 * retorna String com informacoes de cada local
	 * */
	public String getListaLocal() {
		String saida = "";
		for (Local l: this.locais) {
			saida += l.toString();
		}
		return saida;
	}
	
	/*
	 * retorna String com indice e nome do Local
	 * */
	public String getListaLocalIndice() {
		String saida = "";
		
		for (int i = 0; i < this.locais.size(); i++) {
			saida += (i + 1) + " - " + this.locais.get(i).getNome() + "\n";
		}
		
		
		return saida;
	}
	
	/*
	 * overrides toString
	 * */
	public String toString() {
		String saida = "";
		saida += super.toString();
		return saida;
	}
	
	@Override
	public void imprimeDados() {
		System.out.println(this.toString());
		
	}
	
}
