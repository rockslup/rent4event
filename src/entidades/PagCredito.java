package entidades;

import interfaces.Imprimivel;

public class PagCredito extends Pagamento implements Imprimivel {
	
	private int qtdParcelas;
	private double precoParcela;

	public PagCredito(double preco, String numeroCartao, String dataExpiracao,
			String codigoSeguranca, int qtdParcelas) {
		super(preco, numeroCartao, dataExpiracao, codigoSeguranca);
		this.qtdParcelas = qtdParcelas;
		this.precoParcela = (preco / qtdParcelas);
	}

	/**
	 * @return the qtdParcelas
	 */
	public int getQtdParcelas() {
		return qtdParcelas;
	}

	/**
	 * @param qtdParcelas the qtdParcelas to set
	 */
	public void setQtdParcelas(int qtdParcelas) {
		this.qtdParcelas = qtdParcelas;
	}

	/**
	 * @return the precoParcela
	 */
	public double getPrecoParcela() {
		return precoParcela;
	}

	/**
	 * @param precoParcela the precoParcela to set
	 */
	public void setPrecoParcela(double precoParcela) {
		this.precoParcela = precoParcela;
	}
	
	public void imprimeDados() {
		System.out.println("---- Pagamento tipo Credito -------");
		super.imprimeDados();
		String saida = "";
		saida += "Pagamento em " + this.getQtdParcelas() + "x de " + this.getPrecoParcela() + "\n";
		System.out.println(saida);
	}

}
