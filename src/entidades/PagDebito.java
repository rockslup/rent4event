package entidades;

import java.util.Date;

import interfaces.Imprimivel;

public class PagDebito extends Pagamento implements Imprimivel {
	
	private Date dataPagamento;
	
	public PagDebito(double preco, String numeroCartao, String dataExpiracao, String codigoSeguranca) {
		super(preco, numeroCartao, dataExpiracao, codigoSeguranca);
		this.setDataPagamento(new Date());
	}

	/**
	 * @return the dataPagamento
	 */
	public Date getDataPagamento() {
		return dataPagamento;
	}

	/**
	 * @param dataPagamento the dataPagamento to set
	 */
	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public void imprimeDados() {
		System.out.println("---- Pagamento tipo D�bito -------");
		super.imprimeDados();
		String saida = "";
		saida += "Data de Pagamento: " + Pagamento.dateFormat.format(this.getDataPagamento()) + "\n";
		System.out.println(saida);
	}
	

}
