package entidades;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import interfaces.Imprimivel;

public abstract class Pagamento implements Imprimivel {
	private static int sequence = 0;
	private final int ID;
	private double precoTotal;
	private String numeroCartao;
	private String dataExpiracaoCartao;
	private String CVV;
	private Date dataCompra;
	protected static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	public Pagamento(double preco, String numeroCartao, String dataExpiracao, String codigoSeguranca) {
		this.ID = ++(Pagamento.sequence);
		this.precoTotal = preco;
		this.numeroCartao = numeroCartao;
		this.dataExpiracaoCartao = dataExpiracao;
		this.CVV = codigoSeguranca;
		this.dataCompra = new Date();
	}

	
	public int getID() {
		return this.ID;
	}
	
	
	public void setPrecoTotal(double preco) {
		this.precoTotal = preco;
	}
	
	public double getPrecoTotal() {
		return this.precoTotal;
	}

	/**
	 * @return the sequence
	 */
	public static int getSequence() {
		return sequence;
	}

	/**
	 * @param sequence the sequence to set
	 */
	public static void setSequence(int sequence) {
		Pagamento.sequence = sequence;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public String getDataExpiracaoCartao() {
		return dataExpiracaoCartao;
	}

	public void setDataExpiracaoCartao(String dataExpiracaoCartao) {
		this.dataExpiracaoCartao = dataExpiracaoCartao;
	}

	public String getCVV() {
		return CVV;
	}

	public void setCVV(String cVV) {
		CVV = cVV;
	}
	
	public void imprimeDados() {
		String saida = "";
		saida += "Preco Total: " + this.getPrecoTotal() + "\n";
		saida += "Numero Cartao: " + this.getNumeroCartao() + "\n";
		saida += "Data Expiracao Cartao: " + this.getDataExpiracaoCartao() + "\n";
		saida += "Codigo Seguranca: " + this.getCVV() + "\n";
		saida += "Data da Compra: " + Pagamento.dateFormat.format(this.getDataCompra()) + "\n";
		System.out.println(saida);
	}


	/**
	 * @return the dataCompra
	 */
	public Date getDataCompra() {
		return dataCompra;
	}


	/**
	 * @param dataCompra the dataCompra to set
	 */
	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}
	
	

}
