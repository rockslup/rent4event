package entidades;
import java.util.Date;

import interfaces.Imprimivel;

public class Aluguel implements Imprimivel {
	private static int sequence = 0;
	private final int ID;
	private Cliente cliente;
	private Local local;
	private Date dtEntrada;
	private Date dtSaida;
	private Pagamento pagamento;
	private statusAluguel status;
	public enum statusAluguel {
		AGENDADO, ATIVO, CONCLUIDO
	}
	
	public statusAluguel getStatus() {
		this.setStatus(status);
		return status;
	}
	

	public void setStatus(statusAluguel status) {
		
		Date todayDate = new Date();
		if(todayDate.after(this.dtEntrada) && todayDate.before(this.dtSaida)) {
		    // In between
			this.status = statusAluguel.ATIVO;
		}
		else if (todayDate.after(dtEntrada) && todayDate.after(dtSaida)) {
			// ja passou 
			this.status = statusAluguel.CONCLUIDO;
		}
		else {
			this.status = status;
		}
	}
	
	public Aluguel(Cliente cliente, Local local, Date dtEntrada, Date dtSaida, Pagamento pagamento) {
		this.ID = ++(Aluguel.sequence);
		this.cliente = cliente;
		this.local = local;
		this.setDtEntrada(dtEntrada);
		this.setDtSaida(dtSaida);
		this.pagamento = pagamento;
	}
	
	public Aluguel(Cliente cliente, Local local, Date dtEntrada, Date dtSaida) {
		this.ID = ++(Aluguel.sequence);
		this.cliente = cliente;
		this.local = local;
		this.setDtEntrada(dtEntrada);
		this.setDtSaida(dtSaida);
	}

	/**
	 * @return the sequence
	 */
	public static int getSequence() {
		return sequence;
	}

	/**
	 * @param sequence the sequence to set
	 */
	public static void setSequence(int sequence) {
		Aluguel.sequence = sequence;
	}

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the local
	 */
	public Local getLocal() {
		return local;
	}

	/**
	 * @param local the local to set
	 */
	public void setLocal(Local local) {
		this.local = local;
	}

	/**
	 * @return the dtEntrada
	 */
	public Date getDtEntrada() {
		return dtEntrada;
	}

	/**
	 * @param dtEntrada the dtEntrada to set
	 */
	public void setDtEntrada(Date dtEntrada) {
		if(checkDate(dtEntrada)) 
			this.dtEntrada = dtEntrada;
	}

	/**
	 * @return the dtSaida
	 */
	public Date getDtSaida() {
		return dtSaida;
	}

	/**
	 * @param dtSaida the dtSaida to set
	 */
	public void setDtSaida(Date dtSaida) {
		if(checkDate(dtSaida))
			this.dtSaida = dtSaida;
	}

	/**
	 * @return the pagamento
	 */
	public Pagamento getPagamento() {
		return pagamento;
	}

	/**
	 * @param pagamento the pagamento to set
	 */
	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	
	private boolean checkDate(Date date) {
		//TODO
		return true;
	}
	
	public String toString() {
		String saida = "### Aluguel ###\n";
		saida += "ID: " + this.getID() + "\n";
		saida += "Cliente: " + this.cliente.getNome() + "\n";
		saida += "Local: " + this.local.getNome() + "\n";
		saida += "Data Entrada: " + this.getDtEntrada() + "\n";
		saida += "Data Saida: " + this.getDtSaida() + "\n";
		saida += "Status: " + this.getStatus() + "\n";
		return saida;
		
	}
	
	public void imprimeDados() {
		System.out.println(this.toString());
		this.getPagamento().imprimeDados();
	}
}
