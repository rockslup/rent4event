package entidades;

import interfaces.Imprimivel;

public class Endereco implements Imprimivel {
	private static int sequence = 0;
	private final int ID;
	private String logradouro;
	private String numero;
	private String CEP;
	private String bairro;
	private String cidade;
	private String estado;
	private String pais;
	
	public Endereco(String log, String numero, String cep, String bairro, String cidade, String estado, String pais) {
		this.ID = ++(Endereco.sequence);
		this.setLogradouro(log);
		this.setNumero(numero);
		this.setCEP(cep);
		this.setBairro(bairro);
		this.setCidade(cidade);
		this.setEstado(estado);
		this.setPais(pais);
	}

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @return the logradouro
	 */
	public String getLogradouro() {
		return logradouro;
	}

	/**
	 * @param logradouro the logradouro to set
	 */
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}

	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	/**
	 * @return the cEP
	 */
	public String getCEP() {
		return CEP;
	}

	/**
	 * @param cEP the cEP to set
	 */
	public void setCEP(String cep) {
		CEP = cep;
	}
	
	public String toString() {
		String saida = "";
		saida += "Logradouro: " + this.getLogradouro() + "\n";
		saida += "Numero: " + this.getNumero() + "\n";
		saida += "CEP: " + this.getCEP() + "\n";
		saida += "Bairro: " + this.getBairro() + "\n";
		saida += "Cidade: " + this.getCidade() + "\n";
		saida += "Estado: " + this.getEstado() + "\n";
		saida += "Pais: " + this.getPais() + "\n";
		return saida;
	}
	
	@Override
	public void imprimeDados() {
		System.out.println(this.toString());
		
	}
	
}
