package entidades;
import java.util.ArrayList;

import interfaces.Imprimivel;

public class Cliente extends Usuario implements Imprimivel {
	private ArrayList<Aluguel> alugueis = new ArrayList<Aluguel>();
	private String numeroCartao;

	public Cliente(String nome, String cpf, String senha, String email, String tel) {
		super(nome, cpf, senha, email, tel);
	}
	
	public Cliente(String nome, String cpf, String senha, String email, String tel, String numConta, String numCartao) {
		super(nome, cpf, senha, email, tel, numConta);
		this.numeroCartao = numCartao;
	}

	/**
	 * @return the numeroCartao
	 */
	public String getNumeroCartao() {
		return numeroCartao;
	}

	/**
	 * @param numeroCartao the numeroCartao to set
	 */
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	
	public ArrayList<Aluguel> getAlugueis() {
		return alugueis;
	}

	public void setAlugueis(ArrayList<Aluguel> alugueis) {
		this.alugueis = alugueis;
	}
	
	public void addLocal(Aluguel alug) {
		this.alugueis.add(alug);
	}
	
	public void removeLocal(Aluguel alug) {
		this.alugueis.remove(alug);
	}
	
	@Override
	public String toString() {
		String saida = "";
		saida += super.toString();
		//saida += "Numero do Cartao: " + this.getNumeroCartao() + "\n";
		return saida;
	}

	@Override
	public void imprimeDados() {
		System.out.println(this.toString());
		
	}
	

}
