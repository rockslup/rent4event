package entidades;

import interfaces.Imprimivel;

public abstract class Usuario implements Imprimivel {
	private static int sequence = 0;
	private final int ID;
	private String nome;
	private String CPF;
	private String senha;
	private String email;
	private String telefone;
	private String numeroConta;
	
	public Usuario(String nome, String cpf, String senha, String email, String tel, String numConta) {
		this.ID = ++(Usuario.sequence);
		this.setNome(nome);
		this.setCPF(cpf);
		this.setSenha(senha);
		this.setEmail(email);
		this.setTelefone(tel);
		this.setNumeroConta(numConta);
	}
	
	public Usuario(String nome, String cpf, String senha, String email, String tel) {
		this.ID = ++(Usuario.sequence);
		this.setNome(nome);
		this.setCPF(cpf);
		this.setSenha(senha);
		this.setEmail(email);
		this.setTelefone(tel);
	}
	
	/**
	 * @return the sequence (total de usuarios)
	 */
	public int getSequence() {
		return Usuario.sequence;
	}

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the cPF
	 */
	public String getCPF() {
		return CPF;
	}

	/**
	 * @param cPF the cPF to set
	 */
	public void setCPF(String cPF) {
		CPF = cPF;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * @return the telefone
	 */
	public String getTelefone() {
		return telefone;
	}

	/**
	 * @param telefone the telefone to set
	 */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	/**
	 * @return the numeroConta
	 */
	public String getNumeroConta() {
		return numeroConta;
	}

	/**
	 * @param numeroConta the numeroConta to set
	 */
	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}
	
	@Override
	public String toString() {
		String saida = "";
		saida += "ID: " + this.getID() + "\n";
		saida += "Nome: " + this.getNome() + "\n";
		saida += "CPF: " + this.getCPF() + "\n";
		saida += "Email: " + this.getEmail() + "\n";
		saida += "Telefone: " + this.getTelefone() + "\n";
		//saida += "Numero da Conta: " + this.getNumeroConta() + "\n";
		return saida;
	}
	
	@Override
	public void imprimeDados() {
		System.out.println(this.toString());
		
	}
	
}
