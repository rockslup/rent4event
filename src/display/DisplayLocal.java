package display;

import java.util.ArrayList;
import java.util.InputMismatchException;

import entidades.Endereco;
import entidades.Local;
import entidades.Proprietario;
import gerenciadores.GerenciadorLocal;

public class DisplayLocal extends Display {
	public static Local criarLocal(Proprietario proprietario) {
		String nome, log, numero, CEP, bairro, cidade, estado, pais;
		double preco = 0.0;
		boolean status, flag = true;

		System.out.println("--Formulario para cadastro de local--");
		System.out.println("Nome do Local:");
		nome = scan.nextLine();

		do {
			System.out.println("Preco por hora:");
			try {
				preco = scan.nextDouble();
				flag = false;
			} catch (InputMismatchException e) {
				System.out.println("Entrada invalida!");
			} finally {
				scan.nextLine();
			}
		} while (flag);

		int opStatus = 0;
		flag = true;
		do {
			System.out.println("O local ja esta disponivel para ser alugado? ([1] Sim/ [2] Nao)");
			try {
				opStatus = scan.nextInt();
				if(opStatus == 1 || opStatus == 2)
					flag = false;
				else
					System.out.println("Entrada invalida!");
			} catch (InputMismatchException e) {
				System.out.println("Entrada invalida!");
			} finally {
				scan.nextLine();
			}
		} while (flag);

		if (opStatus == 1) {
			status = true;
		} else {
			status = false;
		}

		System.out.println("-- Endereco do Local --");
		System.out.println("Logradouro:");
		log = scan.nextLine();

		System.out.println("Numero:");
		numero = scan.nextLine();

		System.out.println("CEP:");
		CEP = scan.nextLine();

		System.out.println("Bairro:");
		bairro = scan.nextLine();

		System.out.println("Cidade:");
		cidade = scan.nextLine();

		System.out.println("Estado:");
		estado = scan.nextLine();

		System.out.println("Pais:");
		pais = scan.nextLine();

		Endereco e = new Endereco(log, numero, CEP, bairro, cidade, estado, pais);
		Local l = new Local(nome, preco, status, proprietario, e);
		System.out.println("Local criado com sucesso!\n" + l);
		return l;
	}

	public static void mostrarLocais(ArrayList<Local> lista) {
		int i = 1;
		for (Local l : lista) {
			System.out.println("Local [" + (i) + "]\n" + l.toString() + "---------------");
			i++;
		}
	}

	public static void mostrarRemoverLocal(Proprietario proprietario, GerenciadorLocal gerLocal) {
		boolean flag = true;
		int indice = 0;
		ArrayList<Local> locais = proprietario.getLocais();
		mostrarLocais(locais);

		do {
			System.out.println("Insira o indice do local a ser removido ([0] para cancelar): ");
			try {
				indice = scan.nextInt();
				if(indice > locais.size() && indice != 0)
					System.out.println("Entrada invalida");
				else
					flag = false;
			} catch (InputMismatchException e) {
				System.out.println("Entrada invalida");
			} finally {
				scan.nextLine();
			}
		} while (flag);
		
		if(indice != 0) {
			Local l = locais.get((indice - 1));
			proprietario.removeLocal(l);
			gerLocal.removerEntidade(l);
			System.out.println("Local removido com sucesso!");
		}else {
			System.out.println("Operacao cancelada");
		}
		

	}

	public static void mostrarAlterarLocal(Proprietario proprietario) {
		boolean flag = true;
		ArrayList<Local> locais = proprietario.getLocais();
		mostrarLocais(locais);
		int indice = 0;

		do {
			System.out.println("Insira o indice do local a ser alterado: ");
			try {
				indice = scan.nextInt();
				flag = false;
			} catch (InputMismatchException e) {
				System.out.println("Entrada invalida");
			} finally {
				scan.nextLine();
			}
		} while (flag);

		if ((indice > 0) && (locais.size() >= indice)) {
			Local l = locais.get((indice - 1));
			Endereco endereco = l.getEndereco();

			System.out.println("Nome do Local:");
			l.setNome(scan.nextLine());

			System.out.println("Preco por hora:");
			l.setPreco(scan.nextDouble());

			int opStatus = 0;
			do {
				System.out.println("O local ja esta disponivel para ser alugado? ([1] Sim/ [2] Nao)");
				try {
					opStatus = scan.nextInt();
					if (opStatus == 1 || opStatus == 2)
						flag = false;
					else
						System.out.println("Entrada invalida");
				} catch (InputMismatchException e) {
					System.out.println("Entrada invalida");
				} finally {
					scan.nextLine();
				}
			} while (flag);

			if (opStatus == 1) {
				l.setStatus(true);
			} else {
				l.setStatus(false);
			}

			System.out.println("---- Endereco do Local ----");
			System.out.println("Logradouro:");
			endereco.setLogradouro(scan.nextLine());
			System.out.println("Numero:");
			endereco.setNumero(scan.nextLine());
			System.out.println("CEP:");
			endereco.setCEP(scan.nextLine());
			System.out.println("Bairro:");
			endereco.setBairro(scan.nextLine());
			System.out.println("Cidade:");
			endereco.setCidade(scan.nextLine());
			System.out.println("Estado:");
			endereco.setEstado(scan.nextLine());
			System.out.println("Pais:");
			endereco.setPais(scan.nextLine());
			l.setEndereco(endereco);

			System.out.println("Local alterado com sucesso!\n" + l);
		} else {
			System.out.println("Indice invalido!");
		}
	}
}
