package display;

import java.util.InputMismatchException;

import entidades.Cliente;

public class DisplayCliente extends Display {

	public static Cliente criarCliente() {
		String nome, cpf, telefone, email, senha;
		boolean isValid;

		System.out.println("--Formulario para cadastro de cliente--");

		System.out.println("Digite o nome:");
		nome = scan.nextLine();

		do {
			System.out.println("Digite o CPF:");
			cpf = scan.nextLine();
			isValid = validarCpf(cpf);
			if (isValid == false) {
				System.out.println("CPF invalido!");
			}
		} while (isValid == false);

		System.out.println("Digite o telefone:");
		telefone = scan.nextLine();

		do {
			System.out.println("Digite o email:");
			email = scan.nextLine();
			isValid = validarEmail(email);
			if (isValid == false) {
				System.out.println("Email invalido!");
			}
		} while (isValid == false);

		boolean flag = true;
		do {
			System.out.println("Digite a senha:");
			senha = scan.nextLine();
			System.out.println("Confirme a senha:");
			String aux = scan.nextLine();
			if (!senha.equals(aux)) {
				System.out.println("Senha invalida!");
			} else {
				flag = false;
			}
		} while (flag);

		Cliente c = new Cliente(nome, cpf, senha, email, telefone);
		System.out.println("Cliente criado com sucesso!\n" + c);
		return c;
	}

	public static String[] loginCliente() {
		String retorno[] = new String[2];
		String email, senha = "";

		System.out.println("Digite o email:");
		email = scan.nextLine();

		if (!validarEmail(email)) {
			System.out.println("Email Invalido!");
		} else {
			System.out.println("Digite a senha");
			senha = scan.nextLine();
		}

		retorno[0] = email;
		retorno[1] = senha;
		return retorno;
	}

	public static int menuCliente(Cliente cliente) {
		int operacao = 1;
		System.out.println("##Bem vindo " + cliente.getNome() + "##");
		System.out.println("Escolha uma opcao:");
		System.out.println("[1] - Alugar um Local");
		System.out.println("[2] - Alterar Perfil");
		System.out.println("[3] - Visualizar Alugueis");
		System.out.println("[0] - Sair");
		try {
			operacao = scan.nextInt();

		} catch (InputMismatchException e) {
			operacao = OPERACAO_INVALIDA;
		} finally {
			scan.nextLine();
		}
		return operacao;
	}

	public static void mostrarAlterarPerfil(Cliente cliente) {
		boolean flag = true;
		boolean isValid;

		while (flag) {
			int opcao = 0;
			boolean flag2 = true;
			do {
				System.out.println("Digite o código do que deseja alterar:\n" + "[1] - Nome\n" + "[2] - CPF\n"
						+ "[3] - Senha\n" + "[4] - Email\n" + "[5] - Telefone\n" + "[6] - Número do cartão\n"
						+ "[7] - Número da conta\n" + "[0] - Cancelar");
				try {
					opcao = scan.nextInt();
					flag2 = false;
				} catch (InputMismatchException e) {
					opcao = OPERACAO_INVALIDA;
					System.out.println("Operacao invalida");
				} finally {
					scan.nextLine();
				}
			} while (flag2);

			String aux;
			switch (opcao) {
			case 1:
				System.out.println("Nome atual: [" + cliente.getNome() + "]. Digite o novo nome:");
				aux = scan.nextLine();
				cliente.setNome(aux);
				System.out.println("Novo nome: " + cliente.getNome());
				break;
			case 2:
				do {
					System.out.println("CPF atual: [" + cliente.getCPF() + "]. Digite o novo CPF:");
					aux = scan.nextLine();
					isValid = validarCpf(aux);
					if (isValid == false) {
						System.out.println("CPF invalido!");
					}
				} while (isValid == false);
				cliente.setCPF(aux);
				System.out.println("Novo CPF :" + cliente.getCPF());
				break;
			case 3:
				flag2 = true;
				do {
					System.out.println("Senha atual: [" + cliente.getSenha() + "]. Digite a nova senha:");
					aux = scan.nextLine();
					System.out.println("Confirme a nova senha:");
					String aux2 = scan.nextLine();
					if (aux.equals(aux2)) {
						cliente.setSenha(aux);
						System.out.println("Senha alterada com sucesso!");
						flag2 = false;
					} else {
						System.out.println("Senha invalida!");
					}
				} while (flag2);
				break;
			case 4:
				do {
					System.out.println("Email atual: [" + cliente.getEmail() + "]. Digite o novo email:");
					aux = scan.nextLine();
					isValid = validarEmail(aux);
					if (isValid == false) {
						System.out.println("Email invalido!");
					}
				} while (isValid == false);
				cliente.setEmail(aux);
				System.out.println("Email novo: " + cliente.getEmail());
				break;
			case 5:
				System.out.println("Telefone atual: [" + cliente.getTelefone() + "]. Digite o novo telefone:");
				aux = scan.nextLine();
				cliente.setTelefone(aux);
				System.out.println("Telefone novo: " + cliente.getTelefone());
				break;
			case 6:
				System.out.println(
						"Número do cartao atual: [" + cliente.getNumeroCartao() + "]. Digite o novo numero do cartao:");
				aux = scan.nextLine();
				cliente.setNumeroCartao(aux);
				System.out.println("Novo numero do cartao: " + cliente.getNumeroCartao());
				break;
			case 7:
				System.out.println(
						"Numero da conta atual: [" + cliente.getNumeroConta() + "]. Digite o novo numero da conta:");
				aux = scan.nextLine();
				cliente.setNumeroConta(aux);
				System.out.println("Novo numero da conta: " + cliente.getNumeroConta());
				break;
			case 0:
				flag = false;
				break;
			default:
				System.out.println("Operacao invalida");
			}
		}
	}

}
