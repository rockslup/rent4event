package display;

import java.util.InputMismatchException;

public class DisplayInicio extends Display{
	
	public static int mostrarMenuInicial(){
		int operacao;
		System.out.println(" -- WELCOME TO RENT4EVENT --\n");
		System.out.println("Escolha uma opcao:");
		System.out.println("[1] - Cadastrar Cliente");
		System.out.println("[2] - Cadastrar Proprietario");
		System.out.println("[3] - Entrar como Cliente");
		System.out.println("[4] - Entrar como Proprietario");
		System.out.println("[0] - Fechar");
		try {
			operacao = scan.nextInt();
			
		}catch(InputMismatchException e) {
			operacao = OPERACAO_INVALIDA;
		}
		finally {
			scan.nextLine();
		}
		return operacao;
	}
}
