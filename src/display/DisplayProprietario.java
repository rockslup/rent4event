package display;

import java.util.InputMismatchException;

import entidades.Proprietario;

public class DisplayProprietario extends Display {

	public static Proprietario criarProprietario() {
		String nome, cpf, telefone, conta, email, senha;
		boolean isValid;

		System.out.println("--Formulario para cadastro de proprietario--");

		System.out.println("Digite o nome:");
		nome = scan.nextLine();

		do {
			System.out.println("Digite o CPF:");
			cpf = scan.nextLine();
			isValid = validarCpf(cpf);
			if (isValid == false) {
				System.err.println("CPF invalido!");
			}
		} while (isValid == false);

		System.out.println("Digite o telefone:");
		telefone = scan.nextLine();

		System.out.println("Digite o numero da conta:");
		conta = scan.nextLine();

		do {
			System.out.println("Digite o email:");
			email = scan.nextLine();
			isValid = validarEmail(email);
			if(!isValid)
				System.out.println("Email invalido!");
		} while (isValid == false);

		boolean flag = true;
		do {
			System.out.println("Digite a senha:");
			senha = scan.nextLine();
			System.out.println("Confirme a senha:");
			String aux = scan.nextLine();
			if (!senha.equals(aux)) {
				System.out.println("Senha invalida!");
			} else {
				flag = false;
			}
		} while (flag);
		
		Proprietario p = new Proprietario(nome, cpf, senha, email, telefone, conta);
		System.out.println("Proprietario criado com sucesso!\n" + p);
		return p;
	}

	public static String[] loginProprietario() {
		String retorno[] = new String[2];
		String email, senha = "";

		System.out.println("Digite o email:");
		email = scan.nextLine();

		if (!validarEmail(email)) {
			System.err.println("Email Invalido!");
		} else {
			System.out.println("Digite a senha");
			senha = scan.nextLine();
		}

		retorno[0] = email;
		retorno[1] = senha;
		return retorno;
	}

	public static int menuProprietario(Proprietario proprietario) {
		int operacao = 1;
		System.out.println("##Bem vindo " + proprietario.getNome() + "##");
		System.out.println("Escolha uma opcao:");
		System.out.println("[1] - Cadastrar Local");
		System.out.println("[2] - Exibir Locais");
		System.out.println("[3] - Apagar Local");
		System.out.println("[4] - Alterar Local");
		System.out.println("[5] - Visualizar Alugueis");
		System.out.println("[6] - Alterar Perfil");
		System.out.println("[0] - Sair");
		try {
			operacao = scan.nextInt();

		} catch (InputMismatchException e) {
			operacao = OPERACAO_INVALIDA;
		} finally {
			scan.nextLine();
		}
		return operacao;
	}

	public static void mostrarAlterarProprietario(Proprietario prop) {
		boolean flag = true, flag2 = true;
		boolean isValid;

		while (flag) {
			int opcao = 0;
			do {
				System.out.println("Digite o codigo do que deseja alterar:\n" + "[1] - Nome\n" + "[2] - CPF\n" + "[3] - Senha\n" + "[4] - Email\n" + "[5] - Telefone\n" + "[6] - Número da conta\n" + "[0] - Cancelar");
				try {
					opcao = scan.nextInt();
					flag2 = false;
				}catch(InputMismatchException e) {
					System.out.println("Operacao invalida!");
					opcao = OPERACAO_INVALIDA;
				}
				finally {
					scan.nextLine();
				}
			}while(flag2);
			
			String aux;
			switch (opcao) {
			case 1:
				System.out.println("Nome atual: [" + prop.getNome() + "]. Digite o novo nome:");
				aux = scan.nextLine();
				prop.setNome(aux);
				System.out.println("Novo nome: " + prop.getNome());
				break;
			case 2:
				do {
					System.out.println("CPF atual: [" + prop.getCPF() + "]. Digite o novo CPF:");
					aux = scan.nextLine();
					isValid = validarCpf(aux);
					if (isValid == false) {
						System.out.println("CPF invalido!\n");
					}
				} while (isValid == false);
				prop.setCPF(aux);
				System.out.println("Novo CPF: " + prop.getCPF());
				break;
			case 3:
				flag2 = true;
				do {
					System.out.println("Senha atual: [" + prop.getSenha() + "]. Digite a nova senha:");
					aux = scan.nextLine();
					System.out.println("Confirme a nova senha:");
					String aux2 = scan.nextLine();
					if (aux.equals(aux2)) {
						prop.setSenha(aux);
						System.out.println("Senha alterada com sucesso!");
						flag2 = false;
					} else {
						System.out.println("Senha invalida!");
					}
				} while (flag2);
				break;
			case 4:
				do {
					System.out.println("Email atual: [" + prop.getEmail() + "]. Digite o novo email:");
					aux = scan.nextLine();
					isValid = validarEmail(aux);
					if (isValid == false) {
						System.out.println("Email invalido!");
					}
				} while (isValid == false);
				prop.setEmail(aux);
				System.out.println("Novo email: " + prop.getEmail());
				break;
			case 5:
				System.out.println("Telefone atual: [" + prop.getTelefone() + "]. Digite o novo telefone:");
				aux = scan.nextLine();
				prop.setTelefone(aux);
				System.out.println("Novo telefone: " + prop.getTelefone());
				break;
			case 6:
				System.out.println(
						"Numero da conta atual: [" + prop.getNumeroConta() + "]. Digite o novo numero da conta:");
				aux = scan.nextLine();
				prop.setNumeroConta(aux);
				System.out.println("Novo numero da conta: " + prop.getNumeroConta());
				break;
			case 0:
				flag = false;
				break;
			default:
				System.out.println("Operacao invalida");
			}
		}
	}
}
