package display;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;

import entidades.Agenda;
import entidades.Aluguel;
import entidades.Aluguel.statusAluguel;
import entidades.Cliente;
import entidades.Local;
import entidades.PagCredito;
import entidades.PagDebito;
import entidades.Pagamento;
import gerenciadores.GerenciadorAluguel;

public class DisplayAluguel extends Display{
	
	private static DecimalFormat df2 = new DecimalFormat("#,##0.00");
	
	public static void mostrarAlugueis(ArrayList<Aluguel> lista) {
		for (Aluguel a : lista) {
			System.out.println(a.toString());
		}
	}
	
	public static void mostrarFormulario(ArrayList<Local> locais, Cliente cliente, GerenciadorAluguel gerAluguel) {
		boolean flag = true;
		
		//Receber indice do Local a ser alugado
		int indice = 0;
		do {
			System.out.println("Digite o indice do local que deseja alugar :");
			try {
				indice = scan.nextInt();
				if(indice > locais.size())
					System.err.println("Entrada invalida!");
				else
				flag = false;
			} catch(InputMismatchException e) {
				System.err.println("Entrada invalida!");
			}
			
		} while(flag);
		Local l = locais.get(indice - 1);
		
		//Receber a data de entrada do aluguel
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		flag = true;
		Date dtEntrada = null;
		scan.nextLine();
		do {
			
			System.out.println("Digite a data e hora de entrada (dd/mm/aaaa HH:mm):");
			String dtEntradaS = scan.nextLine();
			try {
				// Parsing the String
				dtEntrada = dateFormat.parse(dtEntradaS);
				flag = false;
				if (dtEntrada.before(new Date())) {
					System.err.println("Data invalida");
					flag = true;
				}
			} catch (ParseException e) {
				System.err.println("Data invalida!");
			}
//			finally {
//				scan.nextLine();
//			}
		
		} while(flag);

		
		//Receber a data de saida do aluguel
		flag = true;
		Date dtSaida = null;
		do {
			System.out.println("Digite a data e hora de saida (dd/mm/aaaa HH:mm)");
			String dtSaidaS = scan.nextLine();
			try {
				// Parsing the String
				dtSaida = dateFormat.parse(dtSaidaS);
				flag = false;
				if (dtSaida.before(new Date()) || dtSaida.before(dtEntrada)){
					System.err.println("Data invalida");
					flag = true;
				}
			} catch (ParseException e) {
				System.err.println("Data invalida!");
			}
		} while(flag);
		Aluguel novoAluguel = new Aluguel(cliente, l, dtEntrada, dtSaida);
		try {
			Agenda.findAluguelByDateAndPlace(novoAluguel);
			
			//Calculo do preco do Aluguel
			// in milliseconds
			long diff = dtSaida.getTime() - dtEntrada.getTime();
			
			//converting milliseconds to hours
			double hours =  ((double) diff / (3600000) );
//			System.out.println("Horas de aluguel: " + df2.format(hours));
//			System.out.println("Diff: " + diff);
			long diffDays = diff / (24 * 60 * 60 * 1000);
			//System.out.println("DiffDays: " + diffDays);
			
			// calcula preco total baseado na diferenca de dias e o preco por hora
			double preco = (hours * l.getPreco());
			
			//Confirmacao da reserva
			System.out.println("A reserva de " + diffDays + " dia(s) e " + hours + " hora(s) custa R$" + preco);
			flag = true;
			int confirmacao = 0;
			do {
				System.out.println("Deseja confirmar a reserva? ([1] Sim / [2] Nao)");
				try {
					confirmacao = scan.nextInt();
					if(confirmacao == 1 || confirmacao == 2)
						flag = false;
					else
						System.err.println("Entrada invalida!");
				} catch(InputMismatchException e) {
					System.err.println("Entrada invalida!");
				} finally {
					scan.nextLine();
				}
			} while(flag);
			
			//Forma de pagamento
			if (confirmacao == 1) {
				flag = true;
				
				int formaPag = 0;
				do {
					System.out.println("Digite a forma de pagamento: ([1] Debito / [2] Credito)");
					try {
						formaPag = scan.nextInt();
						if(formaPag == 1 || formaPag == 2)
							flag = false;
						else
							System.err.println("Entrada invalida!");
					} catch(InputMismatchException e) {
						System.err.println("Entrada invalida!");
					} finally {
						scan.nextLine();
					}
				} while(flag);
				
				flag = true;
				String numeroCartao = "";
				
				do {
					System.out.println("Digite o numero do cartao (sem espacos ou pontuacoes):");
					try {
						numeroCartao = scan.next();
						if(numeroCartao.length() >= 12)
							flag = false;
						else
							System.err.println("Entrada invalida!");
					} catch(InputMismatchException e) {
						System.err.println("Entrada invalida!");
					} finally {
						scan.nextLine();
					}
				} while(flag);
				
				flag = true;
				String CVV = "";
				
				do {
					System.out.println("Digite o codigo de seguranca do cartao:");
					try {
						CVV = scan.next();
						if(CVV.length() >= 3)
							flag = false;
						else
							System.err.println("Entrada invalida!");
					} catch(InputMismatchException e) {
						System.err.println("Entrada invalida!");
					} finally {
						scan.nextLine();
					}
				} while(flag);
				
				flag = true;
				String dataExpiracao = "";
				
				do {
					System.out.println("Digite o mes e o ano de expiracao do cartao: (mm/YYYY)");
					try {
						dataExpiracao = scan.next();
						if(dataExpiracao.length() == 7)
							flag = false;
						else
							System.err.println("Entrada invalida!");
					} catch(InputMismatchException e) {
						System.err.println("Entrada invalida!");
					} finally {
						scan.nextLine();
					}
				} while(flag);
				
				Pagamento pagamento;
				if (formaPag == 1) {					
					pagamento = new PagDebito(preco, numeroCartao, dataExpiracao, CVV);
				} else {
					flag = true;
					int qtdParcelas = 0;
					do {
						System.out.println("Digite a quantidade de parcelas que deseja pagar:");
						try {
							qtdParcelas = scan.nextInt();
							if(qtdParcelas > 0)
								flag = false;
							else
								System.err.println("Entrada invalida!");
							
						} catch(InputMismatchException e) {
							System.err.println("Entrada invalida!");
						} catch(Exception e) {
							System.err.println("Entrada invalida!");
						} finally {
							scan.nextLine();
						}
					} while(flag);
					pagamento = new PagCredito(preco, numeroCartao, dataExpiracao, CVV, qtdParcelas);
				}
				pagamento.imprimeDados();


				//adiciona dados do pagamento no aluguel
				novoAluguel.setPagamento(pagamento);
				
				l.addLocal(novoAluguel);
				cliente.addLocal(novoAluguel);
				gerAluguel.addEntidade(novoAluguel);
				novoAluguel.setStatus(statusAluguel.AGENDADO);
				System.out.println("Aluguel efetuado com sucesso!\n");
				novoAluguel.imprimeDados();
			} else {
				System.out.println("Reserva cancelada");
				novoAluguel = null;
			}
			
		} catch (Exception e1) {
			System.err.println(e1.getMessage());
			novoAluguel = null;
		}
	}
	
	public static double mostrarAvaliacao() {
		System.out.println("Digite uma avaliação para o local (1 a 5): ");
		double nota = 0;
		boolean flag = true;
		do {
			try { 
				nota = scan.nextDouble();
				flag = false;
			} catch(InputMismatchException e) {
				System.out.println("Entrada invalida!");
			}
		}while(flag);
		return nota;
	}
}
