package gerenciadores;

import java.util.ArrayList;

import display.Display;
import display.DisplayAluguel;
import display.DisplayCliente;
import display.DisplayLocal;
import entidades.Cliente;
import entidades.Local;

public class GerenciadorCliente extends Gerenciador<Cliente> {
	@Override
	public void ordenaLista() {
		if(getLista().size() != 0) {
			Cliente[] listaAux = new Cliente[getLista().size()];
			for(int i = 0; i < getLista().size(); i++) {
				listaAux[i] = getLista().get(i);
			}
			esvaziarLista();
			Cliente aux;

			for (int i = 0; i < listaAux.length - 1; i++) {
				for (int j = 0; j < listaAux.length - i - 1; j++) {
					if (listaAux[j].getNome().compareTo(listaAux[j + 1].getNome()) > 0) {
						aux = listaAux[j];
						listaAux[j] = listaAux[j + 1];
						listaAux[j + 1] = aux;
					}
				}
			}
			for (Cliente c : listaAux)
				addEntidade(c);
		}
		
	}

	// Método para buscar um cliente por CPF ou email;
	public Cliente buscarCliente(String busca) {
		boolean flag;

		flag = Display.validarCpf(busca);
		if (flag) {
			for (Cliente c : getLista()) {
				if (c.getCPF().equals(busca))
					return c;
			}
		} else {
			flag = Display.validarEmail(busca);
			if (flag) {
				for (Cliente c : getLista()) {
					if (c.getEmail().equals(busca))
						return c;
				}
			}
		}
		return null;
	}

	public Cliente authCliente(String email, String senha) {
		Cliente c = buscarCliente(email);
		if ((c != null) && (c.getSenha().equals(senha))) {
			return c;
		} else {
			return null;
		}
	}

	public void cadastrarCliente() {
		addEntidade(DisplayCliente.criarCliente());
	}

	public void acessarCliente(GerenciadorLocal gerLocal, GerenciadorAluguel gerAluguel) {
		String info[] = new String[2];
		info = DisplayCliente.loginCliente();
		Cliente cliente = authCliente(info[0], info[1]);
		if (cliente != null) {
			int operacao;
			do {
				operacao = DisplayCliente.menuCliente(cliente);
				switch (operacao) {
				case 1:
					alugarLocal(cliente, gerLocal, gerAluguel);
					break;
				case 2:
					alterarCliente(cliente);
					break;
				case 3:
					visualizarAlugueis(cliente);
					break;
				case 0:
					break;
				default:
					System.out.println("Operacao invalida!");
				}
			} while (operacao != 0);
		} else {
			System.out.println("Cliente nao cadastrado no sistema!");
		}
	}
	
	private void alugarLocal(Cliente cliente, GerenciadorLocal gerLocal, GerenciadorAluguel gerAluguel) {
		ArrayList<Local> locais = gerLocal.getLocaisDisponiveis();
		System.out.println("##Locais disponiveis para aluguel##");
		DisplayLocal.mostrarLocais(gerLocal.getLocaisDisponiveis());
		DisplayAluguel.mostrarFormulario(gerLocal.getLocaisDisponiveis(), cliente, gerAluguel);
	}
	
	private void alterarCliente(Cliente cliente) {
		DisplayCliente.mostrarAlterarPerfil(cliente);
	}
	
	private void visualizarAlugueis(Cliente cliente) {
		DisplayAluguel.mostrarAlugueis(cliente.getAlugueis());
	}
}
