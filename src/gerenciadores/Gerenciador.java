package gerenciadores;

import java.util.ArrayList;

public abstract class Gerenciador<E> {
	
	private ArrayList<E> lista = new ArrayList<E>();
	
	public abstract void ordenaLista();
	
	public void addEntidade(E entidade) {
		lista.add(entidade);
	}
	
	public void removerEntidade(E entidade) {
		for(int i = 0; i < lista.size(); i++) {
			if(entidade.equals(lista.get(i))) {
				lista.remove(entidade);
				ordenaLista();
			}
		}
	}
	
	protected void esvaziarLista() {
		lista.removeAll(lista);
	}
	
	public ArrayList<E> getLista(){
		return lista;
	}
	
	public void imprimeLista() {
		for(E entidade : lista) {
			System.out.println(entidade.toString());
		}
	}
}
