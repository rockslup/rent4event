package gerenciadores;

import display.Display;
import display.DisplayAluguel;
import display.DisplayLocal;
import display.DisplayProprietario;
import entidades.Local;
import entidades.Proprietario;

public class GerenciadorProprietario extends Gerenciador<Proprietario> {
	@Override
	public void ordenaLista() {
		if (getLista().size() != 0) {
			Proprietario listaAux[] = new Proprietario[getLista().size()];
			for (int i = 0; i < getLista().size(); i++) {
				listaAux[i] = getLista().get(i);
			}
			Proprietario aux;
			for (int i = 0; i < listaAux.length - 1; i++) {
				for (int j = 0; j < listaAux.length - i - 1; j++) {
					if (listaAux[j].getNome().compareTo(listaAux[j + 1].getNome()) > 0) {
						aux = listaAux[j];
						listaAux[j] = listaAux[j + 1];
						listaAux[j + 1] = aux;
					}
				}
			}
			for (Proprietario p : listaAux)
				addEntidade(p);
		}
	}

	// Método para buscar um proprietario por CPF ou email;
	public Proprietario buscarProprietario(String busca) {
		boolean flag;

		flag = Display.validarCpf(busca);
		if (flag) {
			for (Proprietario p : getLista()) {
				if (p.getCPF().equals(busca))
					return p;
			}
		} else {
			flag = Display.validarEmail(busca);
			if (flag) {
				for (Proprietario p : getLista()) {
					if (p.getEmail().equals(busca))
						return p;
				}
			}
		}
		return null;
	}

	public Proprietario authProprietario(String email, String senha) {
		Proprietario prop = buscarProprietario(email);
		if ((prop != null) && (prop.getSenha().equals(senha))) {
			return prop;
		} else {
			return null;
		}
	}

	public void cadastrarProprietario() {
		addEntidade(DisplayProprietario.criarProprietario());
	}

	public void acessarProprietario(GerenciadorLocal gerLocal) {
		String info[] = new String[2];
		info = DisplayProprietario.loginProprietario();
		Proprietario proprietario = authProprietario(info[0], info[1]);
		if (proprietario != null) {
			int operacao;
			do {
				operacao = DisplayProprietario.menuProprietario(proprietario);
				switch (operacao) {
				case 1:
					cadastrarLocal(proprietario, gerLocal);
					break;
				case 2:
					exibirLocais(proprietario);
					break;
				case 3:
					removerLocal(proprietario, gerLocal);
					break;
				case 4:
					alterarLocal(proprietario);
					break;
				case 5:
					visualizarAlugueisProprietario(proprietario);
					break;
				case 6:
					alterarProprietario(proprietario);
					break;
				case 0:
					break;
				default:
					System.out.println("Operacao invalida!");
				}
			} while (operacao != 0);
		} else {
			System.out.println("Proprietario nao cadastrado no sistema!");
		}
	}

	private void cadastrarLocal(Proprietario proprietario, GerenciadorLocal gerLocal) {
		gerLocal.addEntidade(DisplayLocal.criarLocal(proprietario));
	}

	private void exibirLocais(Proprietario proprietario) {
		System.out.println("-- Lista de Locais do Proprietario --");
		DisplayLocal.mostrarLocais(proprietario.getLocais());
	}

	private void removerLocal(Proprietario proprietario, GerenciadorLocal gerLocal) {
		DisplayLocal.mostrarRemoverLocal(proprietario, gerLocal);
	}

	private void alterarLocal(Proprietario proprietario) {
		exibirLocais(proprietario);
		DisplayLocal.mostrarAlterarLocal(proprietario);
	}

	private void visualizarAlugueisProprietario(Proprietario prop) {
		for (Local local : prop.getLocais()) {
			if (local.getAlugueis().size() > 0) {
				System.out.println("Alugueis de " + local.getNome() + "\n");
				DisplayAluguel.mostrarAlugueis(local.getAlugueis());
			}
		}
	}

	private void alterarProprietario(Proprietario prop) {
		DisplayProprietario.mostrarAlterarProprietario(prop);
	}
}
