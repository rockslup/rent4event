package gerenciadores;

import java.util.ArrayList;

import entidades.Local;

public class GerenciadorLocal extends Gerenciador<Local> {
	@Override
	public void ordenaLista() {
		if (getLista().size() != 0) {
			Local[] listaAux = new Local[getLista().size()];
			for (int i = 0; i < getLista().size(); i++) {
				listaAux[i] = getLista().get(i);
			}
			Local aux;
			for (int i = 0; i < listaAux.length - 1; i++) {
				for (int j = 0; j < listaAux.length - i - 1; j++) {
					if (listaAux[j].getNome().compareTo(listaAux[j + 1].getNome()) > 0) {
						aux = listaAux[j];
						listaAux[j] = listaAux[j + 1];
						listaAux[j + 1] = aux;
					}
				}
			}
			for (Local l : listaAux)
				addEntidade(l);
		}
	}

	public Local buscarLocal(int id) {
		for (Local l : getLista()) {
			if (l.getID() == id)
				return l;
		}
		return null;
	}

	public ArrayList<Local> getLocaisDisponiveis() {
		ArrayList<Local> locaisDisp = new ArrayList<Local>();
		for (Local l : getLista()) {
			if (l.getStatus() == true) {
				locaisDisp.add(l);
			}
		}
		return locaisDisp;
	}
}
